//
//  RocketsCollectionViewCell.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import UIKit

protocol RocketsCollectionViewCellDelegateProtocol {
    
    func populateRocketsCollectionViewCell(with object: RocketElement)
}

final class RocketsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak private var rocketImageView: UIImageView!
    @IBOutlet weak private var rocketNameLabel: UILabel!
    @IBOutlet weak private var firstLaunchLabel: UILabel!
    @IBOutlet weak private var launchCostLabel: UILabel!
    @IBOutlet weak private var successLabel: UILabel!
    
    private let cornerRadius: CGFloat = 16
    private let dollarSign: Character = "$"
    private let percentage: Character = "%"
    
    //MARK: - Variables
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    public static var nib: UINib {
        return UINib(nibName: reuseIdentifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = self.cornerRadius
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.rocketImageView.image = nil
        self.rocketNameLabel.text = nil
        self.firstLaunchLabel.text = nil
        self.launchCostLabel.text = nil
        self.successLabel.text = nil
    }
}

extension RocketsCollectionViewCell: RocketsCollectionViewCellDelegateProtocol {
    
    func populateRocketsCollectionViewCell(with object: RocketElement) {
        self.rocketNameLabel.text = object.name
        self.firstLaunchLabel.text = object.firstFlight
        self.launchCostLabel.text = "\(object.costPerLaunch)\(dollarSign)"
        self.successLabel.text = "\(object.successRatePct)\(percentage)"
        object.flickrImages.forEach { (imageURLString) in
            guard let imageURL = URL(string: imageURLString) else {return}
            self.rocketImageView.load(from: imageURL)
        }
    }
}

