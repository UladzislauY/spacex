//
//  DetailRocketImagesCollectionViewCell.swift
//  SpaceX
//
//  Created by Владислав on 1/27/22.
//

import UIKit

class DetailRocketImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak private var detailRocketImageView: UIImageView!
    
    public class var reuseIdentifier: String {
        return String(describing: self)
    }
    
    public class var nib: UINib {
        return UINib(nibName: reuseIdentifier, bundle: nil)
    }
    
    public var cornerRadius: CGFloat = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = cornerRadius
        self.detailRocketImageView.layer.cornerRadius = cornerRadius
    }
    
    public func populateDetailRocketImagesCollectionViewCell<Object>(with object: Object) where Object : Decodable {
        guard let object = object as? RocketElement else {return}
        object.flickrImages.forEach { (detailRocketImageURLString) in
            guard let detailRocketImageURL = URL(string: detailRocketImageURLString) else {return}
            self.detailRocketImageView.load(from: detailRocketImageURL)
        }
    }
}
