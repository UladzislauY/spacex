//
//  LaunchPadImagesCollectionViewCell.swift
//  SpaceX
//
//  Created by Владислав on 1/28/22.
//

import UIKit

final class LaunchPadImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak private var lauchPadImageView: UIImageView!
    
    public class var reuseIdentifier: String {
        return String(describing: self)
    }
    
    public class var nib: UINib {
        return UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    private let radius: CGFloat = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.radius
        self.lauchPadImageView.layer.cornerRadius = self.radius
        self.baseView.layer.cornerRadius = radius
    }
    
    public func populateDetailRocketImagesCollectionViewCell<Object>(with object: Object) where Object : Decodable {
        guard let object = object as? LaunchPads else {return}
        object.images.large.forEach { (launchPadImageURLString) in
            let launchPadImageURL = URL.constructURL(from: launchPadImageURLString)
            self.lauchPadImageView.load(from: launchPadImageURL)
        }
    }
}
