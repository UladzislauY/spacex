//
//  LaunchesCollectionViewCell.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import UIKit

final class LaunchesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak private var rocketTypeLabel: UILabel!
    @IBOutlet weak private var launchDateLabel: UILabel!
    @IBOutlet weak private var launchImageIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
