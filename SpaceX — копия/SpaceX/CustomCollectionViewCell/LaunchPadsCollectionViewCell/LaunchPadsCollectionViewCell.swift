//
//  LaunchPadsCollectionViewCell.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import UIKit

protocol LaunchPadsCollectionViewCellDelegateProtocol {
    
    func populateLaunchPadsCollectionViewCell(with model: LaunchPads)
}

final class LaunchPadsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak private var launchPadTitleLabel: UILabel!
    @IBOutlet weak private var launchPadLocationLabel: UILabel!
    @IBOutlet weak private var launchPadStatus: UILabel!
    
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    public static var nib: UINib {
        return UINib(nibName: reuseIdentifier, bundle: nil)
    }
    
    private let cornerRadius: CGFloat = 15
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 15
        // Initialization code
    }
}

extension LaunchPadsCollectionViewCell: LaunchPadsCollectionViewCellDelegateProtocol {
    
    func populateLaunchPadsCollectionViewCell(with model: LaunchPads) {
        self.launchPadTitleLabel.text = model.name
        self.launchPadLocationLabel.text = model.region
        self.launchPadStatus.text = model.status
    }
}
