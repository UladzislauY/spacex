//
//  LaunchPadViewController.swift
//  SpaceX
//
//  Created by Владислав on 1/28/22.
//

import UIKit

final class LaunchPadViewController: UIViewController {
    
    @IBOutlet weak private var launchPadCollectionView: UICollectionView!
    
    private let networkManager = NetworkManagerService(networkManager: NetworkManager(), dataParser: ParserManager())
    private var launchPadData: [LaunchPads] = []
    public var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setuplaunchPadCollectionView(delegate: self, dataSource: self)
        getLaunchPadData()
        
    }
    
    private func setuplaunchPadCollectionView(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource) {
        self.launchPadCollectionView.delegate = delegate
        self.launchPadCollectionView.dataSource = dataSource
        self.launchPadCollectionView.register(LaunchPadsCollectionViewCell.nib, forCellWithReuseIdentifier: LaunchPadsCollectionViewCell.reuseIdentifier)
    }
    
    private func getLaunchPadData() {
        guard let launchPadURL = SpaceXAPI.launchpads.url else {return}
        self.networkManager.requestLaunchPads(from: launchPadURL) { (launchPadData) in
            self.launchPadData = launchPadData ?? []
            self.launchPadCollectionView.reloadData()
        }
    }
}

extension LaunchPadViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let selectedLaunchPadData = self.launchPadData[indexPath.item]
        self.coordinator?.pushTpDetailLauchPadViewController(with: selectedLaunchPadData)
    }
}

extension LaunchPadViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.launchPadData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let launchPadCell = collectionView.dequeueReusableCell(withReuseIdentifier: LaunchPadsCollectionViewCell.reuseIdentifier, for: indexPath) as? LaunchPadsCollectionViewCell else {return UICollectionViewCell()}
        launchPadCell.populateLaunchPadsCollectionViewCell(with: launchPadData[indexPath.item])
        return launchPadCell
    }
}


extension LaunchPadViewController: Storyboarded {}
