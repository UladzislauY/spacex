//
//  RocketViewProtocol.swift
//  SpaceX
//
//  Created by Владислав on 1/26/22.
//

import Foundation

protocol RocketViewProtocol {
    
    func retrieveRocketData()
}
