//
//  RocketViewMainPresenter.swift
//  SpaceX
//
//  Created by Владислав on 1/26/22.
//

import Foundation

struct RocketViewMainPresenter: RocketViewPresenterProtocol {
    
    private let networkManager = NetworkManagerService()
    
    public var rocketData = RocketData()
    
    public var rocketDataCount: Int {
        return rocketData.count
    }
    
    func retrieveRocketData(completion: @escaping (RocketData?) -> ()) {
        guard let rocketEndpoint = SpaceXAPI.rockets.url else {return}
        self.networkManager.requestRockets(from: rocketEndpoint, completion: completion)
    }
    
    func getSpecificRocketData(at index: Int) -> RocketElement {
        return rocketData[index]
    }
}
