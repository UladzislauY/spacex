//
//  RocketViewPresenterProtocol .swift
//  SpaceX
//
//  Created by Владислав on 1/26/22.
//

import Foundation

protocol RocketViewPresenterProtocol {
    
    var rocketData: RocketData {get set}
    var rocketDataCount: Int {get}
    func retrieveRocketData(completion: @escaping (RocketData?) -> ())
    func getSpecificRocketData(at index: Int) -> RocketElement
}
