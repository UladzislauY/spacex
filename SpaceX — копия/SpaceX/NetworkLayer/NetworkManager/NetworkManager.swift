//
//  NetworkManager.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import Foundation

public protocol Networking {
    
    func requestData<Data: Decodable>(from url: URL?, with parser: Parsing, completion: @escaping ([Data]?) -> Void)
}

public struct NetworkManager: Networking {
    
    private let urlSession = URLSession.shared
    
}

extension NetworkManager {
    
    public func requestData<Data>(from url: URL?, with parser: Parsing, completion: @escaping ([Data]?) -> Void) where Data : Decodable {
        guard let url = url else {return}
        guard let dataTask = self.createDataTask(with: url, with: parser, completion: completion) else {return}
        dataTask.resume()
    }
    
    private func createDataTask<Data: Decodable>(with url: URL?, with parser: Parsing, completion: @escaping ([Data]?) -> Void) -> URLSessionDataTask? {
        guard let url = url else {return nil}
        return urlSession.dataTask(with: url) { (data, _, _) in
            guard let data = data else {return}
            let object = parser.parseObject(from: data, object: [Data].self)
            DispatchQueue.main.async {
                completion(object)
            }
        }
    }
}
