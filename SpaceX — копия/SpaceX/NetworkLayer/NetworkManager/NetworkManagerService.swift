//
//  NetworkManagerService.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import Foundation

public struct NetworkManagerService {
    
    private let networkManager: Networking
    private let dataParser: Parsing
    
    init(networkManager: Networking = NetworkManager(), dataParser: Parsing = ParserManager()) {
        self.networkManager = networkManager
        self.dataParser = dataParser
    }
    
}

extension NetworkManagerService {
    
    public func requestRockets(from url: URL?, completion: @escaping (RocketData?) -> Void) {
        self.networkManager.requestData(from: url, with: dataParser, completion: completion)
    }
    
    public func requestLaunches(from url: URL?, completion: @escaping ([Launch]?) -> Void) {
        self.networkManager.requestData(from: url, with: dataParser, completion: completion)
    }
    
    public func requestLaunchPads(from url: URL?, completion: @escaping ([LaunchPads]?) -> Void) {
        self.networkManager.requestData(from: url, with: dataParser, completion: completion)
    }
}
