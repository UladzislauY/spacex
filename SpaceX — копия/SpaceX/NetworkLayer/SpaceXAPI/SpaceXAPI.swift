//
//  SpaceXAPI.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import Foundation

public enum SpaceXAPI {
    
    case rockets, launches, launchpads
    
    private enum URLComponent {
        static let scheme: String = "https"
        static let host: String = "api.spacexdata.com"
    }
    
    var url: URL? {
        var components = URLComponents()
        components.scheme = URLComponent.scheme
        components.host = URLComponent.host
        components.path = path
        return components.url
    }
}

extension SpaceXAPI {
    
    fileprivate enum Endpoint {
        
        static let rockets: String = "/v4/rockets"
        static let launches: String = "/v5/launches"
        static let launchpads: String = "/v4/launchpads"
    }
    
    fileprivate var path: String {
        
        switch self {
        
        case .rockets:
            return Endpoint.rockets
        case .launches:
            return Endpoint.launches
        case .launchpads:
            return Endpoint.launchpads
        }
    }
}
