//
//  ParserManager.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import Foundation

public protocol Parsing {
    
    func parseObject<Object: Decodable>(from data: Data, object: Object.Type) -> Object?
}

public struct ParserManager: Parsing {
    
    private let decoder = JSONDecoder()
    
    public func parseObject<Object>(from data: Data, object: Object.Type) -> Object? where Object : Decodable {
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let object = try? decoder.decode(Object.self, from: data)
        return object
    }
}
