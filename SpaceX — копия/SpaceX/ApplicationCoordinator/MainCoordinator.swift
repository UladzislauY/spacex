//
//  MainCoordinator.swift
//  SpaceX
//
//  Created by Владислав on 1/27/22.
//

import UIKit

final class MainCoordinator: Coordinating {
    
    var childCoordinators: [Coordinating] = []
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    public func startCoordinating() {
        let rocketViewController = RocketViewController().instantiate(storyboardName: StoryboardName.rocketStoryboard, viewControllerIdentifier: ViewControllerName.rocketViewController)
        rocketViewController.coordinator = self
        self.navigationController.pushViewController(rocketViewController, animated: false)
    }
    
    public func pushToDetailRocketViewController(with detailRocketData: RocketElement) {
        
        let detailRocketViewController =  DetailRocketViewController().instantiate(storyboardName: StoryboardName.detailRocketStoryboard, viewControllerIdentifier: ViewControllerName.detailRocketViewController)
        detailRocketViewController.detailRocketData.append(detailRocketData)
        detailRocketViewController.coordinator = self
        self.navigationController.pushViewController(detailRocketViewController, animated: true)
    }
    
    
    public func pushToWebViewScene(with url: URL?) {
        let webViewScene = WebViewController().instantiate(storyboardName: StoryboardName.webViewStoryboard, viewControllerIdentifier: ViewControllerName.webViewController)
        webViewScene.wikipediaURL = url
        webViewScene.coordinator = self
        self.navigationController.pushViewController(webViewScene, animated: true)
    }
    
    public func pushToLaunchPadScene() {
        let launchPadScene = LaunchPadViewController().instantiate(storyboardName: StoryboardName.launchPadStoryboard, viewControllerIdentifier: ViewControllerName.launchPadViewController)
        launchPadScene.coordinator = self
        self.navigationController.pushViewController(launchPadScene, animated: true)
    }
    
    public func pushTpDetailLauchPadViewController(with launchPadData: LaunchPads) {
        let detailLauchPadViewController = DetailLaunchPadViewController().instantiate(storyboardName: StoryboardName.detailLaunchPadStoryboard, viewControllerIdentifier: ViewControllerName.detailLaunchPadViewController)
        detailLauchPadViewController.launchPadData.append(launchPadData)
        detailLauchPadViewController.coordinator = self
        self.navigationController.pushViewController(detailLauchPadViewController, animated: true)
    }
}
