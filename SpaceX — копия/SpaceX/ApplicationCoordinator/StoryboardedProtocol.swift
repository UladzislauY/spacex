//
//  StoryboardedProtocol.swift
//  SpaceX
//
//  Created by Владислав on 1/27/22.
//

import UIKit

public protocol Storyboarded {
    
    func instantiate(storyboardName: String, viewControllerIdentifier: String) -> Self
}

extension Storyboarded where Self: UIViewController {
    
    func instantiate(storyboardName: String, viewControllerIdentifier: String) -> Self {
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: .main)
        return storyboard.instantiateViewController(identifier: viewControllerIdentifier) as! Self
    }
}
