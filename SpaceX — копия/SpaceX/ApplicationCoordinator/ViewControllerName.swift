//
//  ViewControllerName.swift
//  SpaceX
//
//  Created by Владислав on 1/27/22.
//

import Foundation

public enum ViewControllerName {
    
    static let rocketViewController: String = "RocketViewController"
    static let detailRocketViewController: String = "DetailRocketViewController"
    static let webViewController: String = "WebViewController"
    static let launchPadViewController: String = "LaunchPadViewController"
    static let detailLaunchPadViewController: String = "DetailLaunchPadViewController"
}
