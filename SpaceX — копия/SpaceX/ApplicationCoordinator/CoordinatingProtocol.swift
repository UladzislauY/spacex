//
//  CoordinatingProtocol.swift
//  SpaceX
//
//  Created by Владислав on 1/27/22.
//

import UIKit

public protocol Coordinating: AnyObject {
    
    var childCoordinators: [Coordinating] {get set}
    var navigationController: UINavigationController {get set}
    
    func startCoordinating()
}
