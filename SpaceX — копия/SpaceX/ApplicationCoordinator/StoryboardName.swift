//
//  StoryboardName.swift
//  SpaceX
//
//  Created by Владислав on 1/27/22.
//

import Foundation

public enum StoryboardName {
    
    static let rocketStoryboard: String = "RocketStoryboard"
    static let detailRocketStoryboard: String = "DetailRocketStoryboard"
    static let webViewStoryboard: String = "WebViewStoryboard"
    static let launchPadStoryboard: String = "LaunchPadStoryboard"
    static let detailLaunchPadStoryboard: String = "DetailLaunchPadStoryboard"
}
