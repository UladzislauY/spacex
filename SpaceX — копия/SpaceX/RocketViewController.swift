//
//  ViewController.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import UIKit

final class RocketViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak private var spaceCollectionView: UICollectionView!
    
    public var presenter: RocketViewPresenterProtocol = RocketViewMainPresenter()
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.retrieveRocketData()
        self.setupSpaceCollectionView(withDelegate: self, withDataSource: self)
        
    }
    
    private func setupSpaceCollectionView(withDelegate: UICollectionViewDelegate, withDataSource: UICollectionViewDataSource) {
        self.spaceCollectionView.delegate = withDelegate
        self.spaceCollectionView.dataSource = withDataSource
        self.spaceCollectionView.register(RocketsCollectionViewCell.nib, forCellWithReuseIdentifier: RocketsCollectionViewCell.reuseIdentifier)
    }
    
    @IBAction private func pushToLaunchPadScene(_ sender: UIButton) {
        self.coordinator?.pushToLaunchPadScene()
    }
}

//MARK: - UICollectionView Extension

extension RocketViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let selectedRocketData = self.presenter.getSpecificRocketData(at: indexPath.item)
        self.coordinator?.pushToDetailRocketViewController(with: selectedRocketData)
    }
}

extension RocketViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.rocketDataCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let spaceCell = collectionView.dequeueReusableCell(withReuseIdentifier: RocketsCollectionViewCell.reuseIdentifier, for: indexPath) as? RocketsCollectionViewCell else {return UICollectionViewCell()}
        spaceCell.populateRocketsCollectionViewCell(with: presenter.getSpecificRocketData(at: indexPath.item))
        return spaceCell
    }
}

//MARK: - Rocket View Protocol Extension
extension RocketViewController: RocketViewProtocol {
    
    func retrieveRocketData() {
        self.presenter.retrieveRocketData { (rocketData) in
            self.presenter.rocketData = rocketData ?? []
            self.spaceCollectionView.reloadData()
        }
    }
}

extension RocketViewController: Storyboarded {}
