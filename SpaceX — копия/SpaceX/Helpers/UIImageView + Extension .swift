//
//  UIImageView + Extension .swift
//  SpaceX
//
//  Created by Владислав on 1/26/22.
//

import UIKit

fileprivate var urlConstant: URL?


extension UIImageView {
    
    func load(from url: URL) {
        urlConstant = url
        self.image = nil
        
        CacherManager.shared.cacheObject(from: url) { (cachedImage) in
            if let imageToCache = cachedImage as? UIImage {
                self.image = imageToCache
            }
        }
        
        let operationQueue = OperationQueue()
        operationQueue.addOperation {
            guard let imageData = try? Data(contentsOf: url) else {return}
            guard let downloadedImage = UIImage(data: imageData) else {return}

            DispatchQueue.main.async {
                if urlConstant == url {
                    CacherManager.shared.saveObject(from: url, object: downloadedImage)
                    self.image = downloadedImage
                }
            }
        }
    }
}
