//
//  URL + Extension.swift
//  SpaceX
//
//  Created by Владислав on 1/28/22.
//

import UIKit

extension URL {
    
    static func constructURL(from urlString: String) -> URL {
        guard let url = URL(string: urlString) else {return .init(fileURLWithPath: "")}
        return url
    }
}
