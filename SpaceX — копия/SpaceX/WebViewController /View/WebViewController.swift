//
//  WebViewController.swift
//  SpaceX
//
//  Created by Владислав on 1/28/22.
//

import UIKit
import WebKit

final class WebViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak private var webView: WKWebView!
    
    //MARK: - Variables
    public var coordinator: MainCoordinator?
    public var wikipediaURL: URL?
    
    //MARK: - ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.openWebViewRequest(from: wikipediaURL)
    }
    
    //MARK: - IBActions
    @IBAction private func moveBackInWebView(_ sender: UIButton) {
        goBackInWebView()
    }
    
    @IBAction private func moveForwardInWebView(_ sender: UIButton) {
        goForwardInWebView()
    }
    
    @IBAction private func shareInformation(_ sender: UIButton) {
        if let sharedURL = wikipediaURL {
            showApplicationActivityView(with: [sharedURL])
        }
    }
    
    @IBAction private func gotToSafariBrowser(_ sender: UIButton) {
        openLinkInSafariBrowser(link: wikipediaURL)
    }
}

extension WebViewController: Storyboarded {}

private extension WebViewController {
    
    func openWebViewRequest(from url: URL?) {
        guard let webURL = url else {return}
        let webRequest = URLRequest(url: webURL)
        self.webView.load(webRequest)
    }
    
    func openLinkInSafariBrowser(link: URL?) {
        if let webURL = link {
            UIApplication.shared.open(webURL)
        }
    }
    
    func showApplicationActivityView(with activityItems: [Any]) {
        let applicationActivityView = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        self.present(applicationActivityView, animated: true, completion: nil)
    }
    
    func goForwardInWebView() {
        
        if webView.canGoForward == true {
            webView.goForward()
        }
    }
    
    func goBackInWebView() {
        if webView.canGoBack == true {
            webView.goBack()
        }
    }
    
    func reloadPage() {
        self.webView.reload()
    }
}
