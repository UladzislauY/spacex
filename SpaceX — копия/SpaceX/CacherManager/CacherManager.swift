//
//  CacherManager.swift
//  SpaceX
//
//  Created by Владислав on 1/26/22.
//

import UIKit

final class CacherManager {
    
    static let shared = CacherManager()
    private init() {}
    
    private var cache = NSCache<NSString, AnyObject>()
    
    public func cacheObject(from url: URL, completion: (AnyObject) -> ()) {
        if let cachedImage = cache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage)
        }
    }
    
    public func saveObject(from url: URL, object: AnyObject) {
        cache.setObject(object, forKey: url.absoluteString as NSString)
    }
}

