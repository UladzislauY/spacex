//
//  Launch.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import Foundation

// MARK: - WelcomeElement
public struct Launch: Decodable {
    let fairings: Fairings?
    let links: Links
//    let staticFireDateUtc: String?
//    let staticFireDateUnix: Int?
//    let net: Bool?
//    let window: Int?
//    let rocket: Rocket
//    let success: Bool?
//    let failures: [Failure]
//    let details: String?
//    let crew, ships, capsules, payloads: [String]?
//    let launchpad: Launchpad
//    let flightNumber: Int?
//    let name, dateUTC: String?
//    let dateUnix: Int?
//    let dateLocal: Date?
//    let datePrecision: DatePrecision
//    let upcoming: Bool?
//    let cores: [Core]
//    let autoUpdate, tbd: Bool?
//    let launchLibraryId: String?
//    let id: String?
}

// MARK: - Core
struct Core: Decodable {
    let core: String?
    let flight: Int?
    let gridfins, legs, reused, landingAttempt: Bool?
    let landingSuccess: Bool?
    let landingType: LandingType?
    let landpad: Landpad?
}

enum LandingType: String, Decodable {
    case asds = "asds"
    case ocean = "ocean"
    case rtls = "rtls"
}

enum Landpad: String, Decodable {
    case the5E9E3032383Ecb267A34E7C7 = "5E9E3032383Ecb267A34E7C7 "
    case the5E9E3032383Ecb554034E7C9 = "5E9E3032383Ecb554034E7C9"
    case the5E9E3032383Ecb6Bb234E7CA = "5E9E3032383Ecb6Bb234E7CA"
    case the5E9E3032383Ecb761634E7Cb = "5E9E3032383Ecb761634E7Cb"
    case the5E9E3032383Ecb90A834E7C8 = "5E9E3032383Ecb90A834E7C8"
    case the5E9E3033383Ecb075134E7CD = "5E9E3033383Ecb075134E7CD"
    case the5E9E3033383Ecbb9E534E7Cc = "5E9E3033383Ecbb9E534E7Cc"
}

enum DatePrecision: String,  Decodable {
    case day = "day"
    case hour = "hour"
    case month = "month"
}

// MARK: - Failure
struct Failure: Decodable {
    let time: Int?
    let altitude: Int?
    let reason: String?
}

// MARK: - Fairings
struct Fairings: Decodable {
    let reused, recoveryAttempt, recovered: Bool?
    let ships: [String]?
}

enum Launchpad: String, Decodable {
    case the5E9E4501F509094Ba4566F84 = "5E9E4501F509094Ba4566F84"
    case the5E9E4502F509092B78566F87 = "5E9E4502F509092B78566F87"
    case the5E9E4502F509094188566F88 = "5E9E4502F509094188566F88"
    case the5E9E4502F5090995De566F86 = "5E9E4502F5090995De566F86"
}

// MARK: - Links
struct Links: Decodable {
    let patch: Patch
    let reddit: Reddit
//    let flickr: Flickr
    let presskit: String?
    let webcast: String?
    let youtubeID: String?
    let article: String?
    let wikipedia: String?
}

// MARK: - Flickr
//struct Flickr: Decodable {
//    let small: [Any?]
//    let original: [String]
//}

// MARK: - Patch
struct Patch: Decodable {
    let small, large: String?
}

// MARK: - Reddit
struct Reddit: Decodable {
    let campaign: String?
    let launch: String?
    let media, recovery: String?
}

enum Rocket: String, Decodable {
    case the5E9D0D95Eda69955F709D1Eb = "5E9D0D95Eda69955F709D1Eb"
    case the5E9D0D95Eda69973A809D1Ec = "5E9D0D95Eda69973A809D1Ec"
    case the5E9D0D95Eda69974Db09D1Ed = "5E9D0D95Eda69974Db09D1Ed"
}
