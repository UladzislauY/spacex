//
//  LaunchPad.swift
//  SpaceX
//
//  Created by Владислав on 1/25/22.
//

import Foundation

public struct LaunchPads: Decodable {
    let images: Images
    let name, fullName, locality, region: String
    let latitude, longitude: Double
    let launchAttempts, launchSuccesses: Int
    let rockets: [String]
    let timezone: String
    let launches: [String]
    let status, details, id: String
}

// MARK: - Images
public struct Images: Decodable {
    let large: [String]
}
