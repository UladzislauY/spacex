//
//  DetailLaunchPadViewController.swift
//  SpaceX
//
//  Created by Владислав on 1/28/22.
//

import UIKit
import MapKit
import CoreLocation

final class DetailLaunchPadViewController: UIViewController{
    
    //MARK: - MapKit IBOutlet
    @IBOutlet weak private var mapView: MKMapView!
    //MARK: - UICollectionView IBOutlet
    @IBOutlet weak private var launchPadImagesCollectionView: UICollectionView!
    //MARK: - LaunchPad Information Section IBOutlets
    @IBOutlet weak private var launchPadModelLabel: UILabel!
    @IBOutlet weak private var launchPadLocationLabel: UILabel!
    @IBOutlet weak private var launchPadStatusLabel: UILabel!
    //MARK: - LaunchPad Description Section IBOutlet
    @IBOutlet weak private var launchPadDescriptionLabel: UILabel!
    //MARK: - LaunchPad Overview Section IBOutlets
    @IBOutlet weak private var launchPadRegionLabel: UILabel!
    @IBOutlet weak private var launchPadFullLocationLabel: UILabel!
    @IBOutlet weak private var launchPadAttemptsLabel: UILabel!
    @IBOutlet weak private var launchPadSuccessLabel: UILabel!
    
    //MARK: - Public Variables
    public var coordinator: MainCoordinator?
    public var launchPadData: [LaunchPads] = []
    private let locationManager = CLLocationManager()
    private var coordinates = (latitude: 0.0, longitude: 0.0)
    private var pinTitle: String = ""
    private let mapViewCornerRadius: CGFloat = 16
    //MARK: - ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLaunchPadImagesCollectionView(delegate: self, dataSource: self)
        self.setDetailLaunchPadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupLocationManager(delegate: self)
    }
}

extension DetailLaunchPadViewController: Storyboarded {}

extension DetailLaunchPadViewController: UICollectionViewDelegate {
    
}

extension DetailLaunchPadViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.launchPadData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let launchPadImagesCell = collectionView.dequeueReusableCell(withReuseIdentifier: LaunchPadImagesCollectionViewCell.reuseIdentifier, for: indexPath) as? LaunchPadImagesCollectionViewCell else {return UICollectionViewCell()}
        launchPadImagesCell.populateDetailRocketImagesCollectionViewCell(with: launchPadData[indexPath.item])
        return launchPadImagesCell
    }
}

private extension DetailLaunchPadViewController {
    
    func setupLaunchPadImagesCollectionView(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource) {
        self.launchPadImagesCollectionView.delegate = delegate
        self.launchPadImagesCollectionView.dataSource = dataSource
        self.launchPadImagesCollectionView.register(LaunchPadImagesCollectionViewCell.nib, forCellWithReuseIdentifier: LaunchPadImagesCollectionViewCell.reuseIdentifier)
    }
    
    func setDetailLaunchPadData() {
        self.launchPadData.forEach { [weak self] (launchPad) in
            DispatchQueue.main.async {
                self?.launchPadModelLabel.text = launchPad.name
                self?.launchPadLocationLabel.text = launchPad.locality
                self?.launchPadStatusLabel.text = launchPad.status
                self?.launchPadDescriptionLabel.text = launchPad.details
                self?.launchPadRegionLabel.text = launchPad.region
                self?.launchPadFullLocationLabel.text = launchPad.region
                self?.launchPadAttemptsLabel.text = String(launchPad.launchAttempts)
                self?.launchPadSuccessLabel.text = String(launchPad.launchSuccesses)
                self?.coordinates.latitude = launchPad.latitude
                self?.coordinates.longitude = launchPad.longitude
                self?.pinTitle = launchPad.region
            }
        }
    }
    
    func setupLocationManager(delegate: CLLocationManagerDelegate) {
        self.locationManager.delegate = delegate
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    
    func renderLaunchPadCurrentLocation(_ location: CLLocation) {
        var coordinates = self.getLaunchPadLocationCoordinates()
        coordinates = location.coordinate
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: coordinates, span: span)
        mapView.setRegion(region, animated: false)
        mapView.addAnnotation(pinAnnotation(with: coordinates))
        mapView.layer.cornerRadius = mapViewCornerRadius
        
    }
    
    func pinAnnotation(with coordinate: CLLocationCoordinate2D) -> MKPointAnnotation {
        let pin = MKPointAnnotation()
        pin.title = pinTitle
        pin.coordinate = coordinate
        return pin
    }
    
    func getLaunchPadLocationCoordinates() -> CLLocationCoordinate2D {
        var coordinate = CLLocationCoordinate2D()
        coordinate.latitude = self.coordinates.latitude
        coordinate.longitude = self.coordinates.longitude
        return coordinate
    }
}

extension DetailLaunchPadViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.locationManager.stopUpdatingLocation()
            self.renderLaunchPadCurrentLocation(location)
        }
    }
}
