//
//  RocketViewHeader.swift
//  SpaceX
//
//  Created by Владислав on 1/30/22.
//

import UIKit

final class RocketViewHeader: UIView {
    
    @IBOutlet weak private var rocketImageView: UIImageView!
    @IBOutlet weak private var rocketNameLabel: UILabel!
    
    private var nibName: String {
        return String(describing: self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
//        instantiate()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadViewFromNib()
//        instantiate()
    }
    
    public func populate(with data: RocketElement) {
        self.rocketNameLabel.text = data.name
        guard let rocketImageURL = URL(string: data.flickrImages.first ?? .init()) else {return}
        self.rocketImageView.load(from: rocketImageURL)
    }
    
    private func loadViewFromNib() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self), owner: self, options: nil)?.first as? UIView else {return}
        
        addSubview(view)
        
//        guard let view = loadViewFromNib() else {return}
//        view.frame = bounds
//        addSubview(view)
    }
    
//    private func loadViewFromNib() -> UIView? {
//        let nib = UINib(nibName: "", bundle: .main)
//        return nib.instantiate(withOwner: self, options: nil).first as? UIView
//    }
}
