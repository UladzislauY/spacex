//
//  ReusableSection.swift
//  SpaceX
//
//  Created by Владислав on 1/30/22.
//

import UIKit

final class ReusableSection: UIView {
    
    //MARK: - Constant
    private enum Constant {
        static let cost: Character = "$"
        static let percentage: Character = "%"
        static let mass: String = "kg"
        static let meters: String = "meters"
    }
    
    //MARK: - Value IBOutlets
    @IBOutlet weak private var firstLaunchLabel: UILabel!
    @IBOutlet weak private var launchCostLabel: UILabel!
    @IBOutlet weak private var launchSuccessLabel: UILabel!
    @IBOutlet weak private var rocketMassLabel: UILabel!
    @IBOutlet weak private var rocketHeightLabel: UILabel!
    @IBOutlet weak private var rocketDiameterLabel: UILabel!
    
    private var nibName: String {
        return String(describing: self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        instantiate()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        instantiate()
    }
    
    public func populate(with object: RocketElement) {
        self.firstLaunchLabel.text = object.firstFlight
        self.launchCostLabel.text = "\(object.costPerLaunch)\(Constant.cost)"
        self.launchSuccessLabel.text = "\(object.successRatePct)\(Constant.percentage)"
        self.rocketMassLabel.text = "\(object.mass.kg) \(Constant.mass)"
        self.rocketHeightLabel.text = "\(object.height.meters ?? .zero) \(Constant.meters)"
        self.rocketDiameterLabel.text = "\(object.diameter.meters ?? .zero) \(Constant.meters)"
        
    }
    
    private func instantiate() {
        guard let view = loadViewFromNib() else {return}
        view.frame = self.bounds
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView? {
        let nib = UINib(nibName: nibName, bundle: Bundle.main)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}
