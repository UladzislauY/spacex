//
//  DetailRocketViewController.swift
//  SpaceX
//
//  Created by Владислав on 1/26/22.
//

import UIKit

final class DetailRocketViewController: UIViewController {
    
    private enum Constant {
        static let meters: String = "meters"
        static let mass: String = "kg"
        static let percentage: Character = "%"
        static let cost: Character = "$"
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var rocketViewHeaderSection: RocketViewHeader!
    
    
//    @IBOutlet weak var descriptionSectionView: ReusableSection!
    //MARK: - OverView Section IBOutlets
//    @IBOutlet weak private var firstLaunchLabel: UILabel!
//    @IBOutlet weak private var launchCostLabel: UILabel!
//    @IBOutlet weak private var successLabel: UILabel!
//    @IBOutlet weak private var massLabel: UILabel!
//    @IBOutlet weak private var heightLabel: UILabel!
//    @IBOutlet weak private var diameterLabel: UILabel!
//
//    //MARK: - Engines Section IBOutlets
//    @IBOutlet weak private var engineTypeLabel: UILabel!
//    @IBOutlet weak private var engineLayoutLabel: UILabel!
//    @IBOutlet weak private var engineVersionLabel: UILabel!
//    @IBOutlet weak private var engineAmoutLabel: UILabel!
//    @IBOutlet weak private var propellantOneLabel: UILabel!
//    @IBOutlet weak private var propellantTwoLabel: UILabel!
//    //MARK: - First Stage Section IBOutlets
//    @IBOutlet weak private var firstStageReusableLabel: UILabel!
//    @IBOutlet weak private var firstStageEnginesAmountLabel: UILabel!
//    @IBOutlet weak private var firstStageFuelAmountLabel: UILabel!
//    @IBOutlet weak private var firstStageBurningTimeLabel: UILabel!
//    @IBOutlet weak private var thrustSeaLevelLabel: UILabel!
//    @IBOutlet weak private var thrustVacuumLabel: UILabel!
//
//    //MARK: - Second Stage Section IBOutlets
//    @IBOutlet weak private var secondStageReusableLabel: UILabel!
//    @IBOutlet weak private var secondStageEnginesAmountLabel: UILabel!
//    @IBOutlet weak private var secondStageFuelAmountLabel: UILabel!
//    @IBOutlet weak private var secondStageBurningTimeLabel: UILabel!
//    @IBOutlet weak private var secondStageThrustLabel: UILabel!
//    //MARK: - Landing Legs Section IBOutlets
//    @IBOutlet weak private var landingLegsAmountLabel: UILabel!
//    @IBOutlet weak private var landingLegsMaterialLabel: UILabel!
//    //MARK: - Images Section IBoutlets
//    @IBOutlet weak private var detailRocketImagesCollectionView: UICollectionView!
//
    //MARK: - Public Variables
    public var detailRocketData: RocketData = RocketData()
    public weak var coordinator: MainCoordinator?
//    private var wikipediaURL: URL?
    //MARK: - ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setDetailRocketData()
        
//        self.setupDetailRocketImagesCollectionView(delegate: self, dataSource: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction private func goToWikipediaWebPage(_ sender: UIButton) {
//        self.coordinator?.pushToWebViewScene(with: wikipediaURL)
    }
}
//MARK: - Storyboared Protocol Extension
extension DetailRocketViewController: Storyboarded {
    
}
//MARK: - DetailRocketViewController Extension
extension DetailRocketViewController {
    
    private func setDetailRocketData() {
        for detailRocketElement in detailRocketData {
                DispatchQueue.main.async { [weak self] in
                    self?.rocketViewHeaderSection.populate(with: detailRocketElement)
//                    self.descriptionSectionView.populate(with: detailRocketElement)
//                    guard let url = URL(string: detailRocketElement.wikipedia) else {return}
//                    self.wikipediaURL = url
//                    self.setDetailRocketImage(from: detailRocketElement.flickrImages)
//                    self.setDescriptionLabelText(text: detailRocketElement.description)
//                    self.setFirstLaunchLabelText(text: detailRocketElement.firstFlight)
//                    self.setLaunchCostLabelText(text: "\(detailRocketElement.costPerLaunch)\(Constant.cost)")
//                    self.setSuccessLabelText(text: "\(detailRocketElement.successRatePct)\(Constant.percentage)")
//                    self.setMassLabelText(text: "\(detailRocketElement.mass.kg) \(Constant.mass)")
//                    self.setHeightLabelText(text: "\(detailRocketElement.height.meters ?? .zero) \(Constant.meters)")
//                    self.setDiameterLabelText(text: "\(detailRocketElement.diameter.meters ?? .zero) \(Constant.meters)")
//
//                    self.setEngineTypeLabelText(text: detailRocketElement.engines.type)
//                    self.setEngineLayoutLabelText(text: detailRocketElement.engines.layout ?? .init())
//                    self.setEngineAmountLabelText(text: "\(detailRocketElement.engines.number)")
//                    self.setEngineVersionLabelText(text: detailRocketElement.engines.version)
//                    self.setPropellantOneLabelText(text: detailRocketElement.engines.propellant1)
//                    self.setPropellantTwoLabelText(text: detailRocketElement.engines.propellant2)
//
//                    self.setReusableLabelText(text: "\(detailRocketElement.firstStage.reusable)")
//                    self.setEnginesAmountLabelText(text: "\(detailRocketElement.firstStage.engines)")
//                    self.setFuelAmountLabelText(text: "\(detailRocketElement.firstStage.fuelAmountTons)")
//                    self.setBurningTimeLabelText(text: "\(detailRocketElement.firstStage.burnTimeSEC ?? .zero)")
//                    self.setThrustSeaLevelLabelText(text: "\(detailRocketElement.firstStage.thrustSeaLevel.kN)")
//                    self.setThrustVacuumLabelText(text: "\(detailRocketElement.firstStage.thrustVacuum.kN)")
//
//                    self.setSecondStageReusableLabelText(text: "\(detailRocketElement.secondStage.reusable)")
//                    self.setSecondStageEnginesAmountLabelText(text: "\(detailRocketElement.secondStage.engines)")
//                    self.setSecondStageFuelAmountLabelText(text: "\(detailRocketElement.secondStage.fuelAmountTons)")
//                    self.setSecondStageBurningTimeLabelText(text: "\(detailRocketElement.secondStage.burnTimeSEC ?? .zero)")
//                    self.setSecondStageThrustLabelText(text: "\(detailRocketElement.secondStage.thrust.kN)")
//
//                    self.setLandingLegsMateriaLabelText(text: detailRocketElement.landingLegs.material ?? .init())
//                    self.setLandingLegsAmountLabelText(text: "\(detailRocketElement.landingLegs.number)")
//
                }
            
        }
    }
    //MARK: - Overview population methods
//    private func setDetailRocketImage(from urls: [String]) {
//        for url in urls {
//            guard let detailRocketImageURL = URL(string: url) else {return}
//            self.detailRocketImageView.load(from: detailRocketImageURL)
//        }
//    }
    
//    private func setDescriptionLabelText(text: String) {
//        self.descriptionLabel.text = text
//    }
////
//    private func setFirstLaunchLabelText(text: String) {
//        self.firstLaunchLabel.text = text
//    }
//
//    private func setLaunchCostLabelText(text: String) {
//        self.firstLaunchLabel.text = text
//    }
//
//    private func setSuccessLabelText(text: String) {
//        self.successLabel.text = text
//    }
//
//    private func setMassLabelText(text: String) {
//        self.massLabel.text = text
//    }
//
//    private func setHeightLabelText(text: String) {
//        self.heightLabel.text = text
//    }
//
//    private func setDiameterLabelText(text: String) {
//        self.diameterLabel.text = text
//    }
//
//    //MARK: - Engines Population Methods
//
//    private func setEngineTypeLabelText(text: String) {
//        self.engineTypeLabel.text = text
//    }
//
//    private func setEngineLayoutLabelText(text: String) {
//        self.engineLayoutLabel.text = text
//    }
//
//    private func setEngineVersionLabelText(text: String) {
//        self.engineVersionLabel.text = text
//    }
//
//    private func setEngineAmountLabelText(text: String) {
//        self.engineAmoutLabel.text = text
//    }
//
//    private func setPropellantOneLabelText(text: String) {
//        self.propellantOneLabel.text = text
//    }
//
//    private func setPropellantTwoLabelText(text: String) {
//        self.propellantTwoLabel.text = text
//    }
//
//    //MARK: - First Stage Section Population Methods
//
//    private func setReusableLabelText(text: String) {
//        self.firstStageReusableLabel.text = text
//    }
//
//    private func setEnginesAmountLabelText(text: String) {
//        self.engineAmoutLabel.text = text
//    }
//
//    private func setFuelAmountLabelText(text: String) {
//        self.firstStageFuelAmountLabel.text = text
//    }
//
//    private func setBurningTimeLabelText(text: String) {
//        self.firstStageBurningTimeLabel.text = text
//    }
//
//    private func setThrustSeaLevelLabelText(text: String) {
//        self.thrustSeaLevelLabel.text = text
//    }
//
//    private func setThrustVacuumLabelText(text: String) {
//        self.thrustVacuumLabel.text = text
//    }
//
//    //MARK: - Second Stage Section Population Methods
//
//    private func setSecondStageReusableLabelText(text: String) {
//        self.secondStageReusableLabel.text = text
//    }
//
//    private func setSecondStageEnginesAmountLabelText(text: String) {
//        self.secondStageEnginesAmountLabel.text = text
//
//    }
//
//    private func setSecondStageFuelAmountLabelText(text: String) {
//        self.secondStageFuelAmountLabel.text = text
//    }
//
//    private func setSecondStageBurningTimeLabelText(text: String) {
//        self.secondStageBurningTimeLabel.text = text
//    }
//
//    private func setSecondStageThrustLabelText(text: String) {
//        self.secondStageThrustLabel.text = text
//    }
//
//    //MARK: - Landing Legs Section Population Methods
//
//    private func setLandingLegsMateriaLabelText(text: String) {
//        self.landingLegsMaterialLabel.text = text
//    }
//
//    private func setLandingLegsAmountLabelText(text: String) {
//        self.landingLegsAmountLabel.text = text
//    }
    
//    private func setupDetailRocketImagesCollectionView(delegate: UICollectionViewDelegate, dataSource: UICollectionViewDataSource) {
//        self.detailRocketImagesCollectionView.delegate = delegate
//        self.detailRocketImagesCollectionView.dataSource = dataSource
//        self.detailRocketImagesCollectionView.register(DetailRocketImagesCollectionViewCell.nib, forCellWithReuseIdentifier: DetailRocketImagesCollectionViewCell.reuseIdentifier)
//    }
}
//MARK: - UICollectionView Extension
//extension DetailRocketViewController: UICollectionViewDelegate {
//
//}
//
//extension DetailRocketViewController: UICollectionViewDataSource {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        self.detailRocketData.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        guard let detailRocketImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailRocketImagesCollectionViewCell.reuseIdentifier, for: indexPath) as? DetailRocketImagesCollectionViewCell else {return UICollectionViewCell()}
//        detailRocketImageCell.populateDetailRocketImagesCollectionViewCell(with: detailRocketData[indexPath.item])
//        self.detailRocketImagesCollectionView.reloadData()
//        return detailRocketImageCell
//    }
//}
